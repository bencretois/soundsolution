#!/bin/usr/env python

import glob
import random
import argparse
import pytorch_lightning as pl
import yaml
import os
import mlflow.pytorch

from itertools import chain
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
#from pytorch_lightning.loggers import TensorBoardLogger
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler, PopulationBasedTraining
from ray.tune.integration.pytorch_lightning import TuneReportCallback, TuneReportCheckpointCallback
from ray.tune.integration.mlflow import mlflow_mixin
from yaml.loader import FullLoader

from utils.augment_audio import AugmentData
from utils.datamodule import EncodeLabels
from utils.datamodule import AudioDataModule
from utils.trainingmodule import TrainingModule
from model_script.simple_model import SimpleModel

@mlflow_mixin
def train_model(config):
    #############################
    # Create the data iterators #
    #############################
    allFiles = [f for f in glob.glob(config["train_val_dataset_path"], recursive=True) if os.path.isfile(f)]

    # Split allFiles into a training / validation split
    train_samples = random.sample(allFiles, int(len(allFiles) * config["proportion_training"]))
    val_samples = [item for item in allFiles if item not in train_samples]

    # Initialize empty iterators that will be populated with augmented files
    data_train = iter(())
    data_val = iter(())

    # Initialize the AugmentData class
    augment_data = AugmentData(p_augmentation=config["p_augmentation"], 
                                l_segments=config["segment_length"], 
                                normalization_value_dBFS=config["normalization_value_dBFS"], 
                                n_fft=config["n_fft"], 
                                hop_length=config["hop_length"], 
                                n_mels=config["n_mels"], 
                                sample_rate=config["sample_rate"])

    # Populate the empty iterators with the augmented data
    for audiofile in train_samples:
        it = augment_data.main(audiofile)
        data_train = chain(data_train, it)

    for audiofile in val_samples:
        it = augment_data.main(audiofile)
        data_val = chain(data_val, it)

    ###########################
    # Create the labelEncoder #
    ###########################
    label_encoder = EncodeLabels(path_to_folders=config["train_val_dataset_path"])

    # Save name of the folder and associated label in a json file
    l = label_encoder.__getLabels__()
    t = label_encoder.class_encode.transform(l)

    for i, j in zip(l,t):
        print("Folder: {}, Associated label: {}".format(i,j))

    ########################
    # Define the callbacks #
    ########################
    early_stopping = EarlyStopping(monitor="val_loss", patience=config["stopping_rule_patience"])

    tune_callback = TuneReportCallback(
    {
        "loss": "val_loss",
        "mean_accuracy": "val_accuracy"
    },
    on="validation_end")

    #########################################################################
    # Instantiate the trainLoader and valLoader and train the model with pl #
    #########################################################################
    trainLoader, valLoader = AudioDataModule(data_train, data_val, label_encoder, 
                                batch_size=config["batch_size"],
                                num_workers=config["num_workers"]).train_val_loader()

    # Customize the training (add GPUs, callbacks ...)
    mlflow.pytorch.autolog(log_models = True)
    trainer = pl.Trainer(default_root_dir=config["path_lightning_metrics"], 
                        #logger=TensorBoardLogger(save_dir=tune.get_trial_dir(), name="", version="."),
                        max_epochs=config["n_epoch"],
                        callbacks=[tune_callback, early_stopping])

    # Parameters for the training loop
    training_loop = TrainingModule(learning_rate=config["learning_rate"])

    # Finally train the model
    #with mlflow.start_run(experiment_id=config["current_experiment"]) as run:
    trainer.fit(training_loop, trainLoader, valLoader)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--config",
                        help="Path to the config file",
                        required=False,
                        default="/app/config.yaml",
                        type=str
    )

    cli_args = parser.parse_args()

    with open(cli_args.config) as f:
        config = yaml.load(f, Loader=FullLoader)

    for key in ('learning_rate', 'batch_size'):
        config[key] = eval(config[key])
    config["mlflow"]["tracking_uri"] = eval(config["mlflow"]["tracking_uri"])

    
    #train_model(config)  # single thread (useful for debugging)

    # Set the MLflow experiment, or create it if it does not exist.
    mlflow.set_tracking_uri(None)
    mlflow.set_experiment("/app/tune_snowscooter")

    scheduler = ASHAScheduler(
        max_t=5,
        grace_period=1,
        reduction_factor=2)

    reporter = CLIReporter(
        parameter_columns=["learning_rate", "batch_size"],
        metric_columns=["loss", "mean_accuracy", "training_iteration"])

    resources_per_trial = {"cpu": 2, "gpu": 0.3}

    trainable = tune.with_parameters(train_model)

    analysis = tune.run(trainable,
        resources_per_trial=resources_per_trial,
        metric="loss",
        mode="min",
        config=config,
        num_samples=3, # Run 10 trials (each trial is one instance of a Trainable) // Number of times to sample from the hyperparameter space
        scheduler=scheduler,
        progress_reporter=reporter,
        name="tune_snowscooter",
        local_dir="/app/.")

    print("Best hyperparameters found were: ", analysis.best_config)

# docker run --rm -it -v ~/Code/soundsolution:/app -v ~/Data/:/Data --gpus all soundsolution:latest poetry run python main_scripts/train_pipeline.py
