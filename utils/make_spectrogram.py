"""
Function to generate a spectrogram

Each column in the spectrogram is the FFT of a slice in time where the centre at this time point has a window placed with n_fft=X components.
n_fft = number of samples per fft
hop length tells us how many audio samples we need to skip over before we calculate the next FFT by default n_fft / 4
Hop length thus controls for the number of columns
    
Our sample rate is 16000Hz, if we take n_fft=1024 we do a fft every 0.064sec
"""

import librosa
import librosa.display
import matplotlib.pyplot as plt
import pandas as pd

def generate_spectrogram(x, sr, n_fft=2048, hop_length=512, show=False):
    """
    A Spectrogram chops up the duration of the sound signal into smaller time segments and 
    then applies the Fourier Transform to each segment, to determine the frequencies contained in that segment.
    """
    X = librosa.stft(x, n_fft=n_fft, hop_length=hop_length)
    Xdb = librosa.amplitude_to_db(abs(X))
    if show == True:
        librosa.display.specshow(Xdb, sr=sr, cmap='viridis', x_axis='time', y_axis='hz')
    return(Xdb)

def generate_mel_spectrogram(x, sr, n_fft=2048, hop_length=512, show=False):

    """
    - It uses the Mel Scale instead of Frequency on the y-axis.
    - It uses the Decibel Scale instead of Amplitude to indicate colors.
    """
    
    sgram = librosa.stft(x, n_fft=n_fft, hop_length=hop_length)
    
    # Separate a complex-valued spectrogram D into its magnitude (S) and phase (P)
    sgram_mag, _ = librosa.magphase(sgram)
    
    # Compute the mel spectrogram -> convert frequency in mel-scale
    mel_scale_sgram = librosa.feature.melspectrogram(S=sgram_mag, sr=sr)
    
    # use the decibel scale to get the final Mel Spectrogram
    mel_sgram = librosa.amplitude_to_db(mel_scale_sgram)
    
    # Display the mel spectrogram
    if show == True:
        librosa.display.specshow(mel_sgram, sr=sr, x_axis='time', y_axis='mel')
        plt.colorbar(format='%+2.0f dB')
        
    return(mel_sgram)

def show_waveplot(y, sr):
    """Plot a waveplot: show the shape of the wave shows the pattern of the vibration"""
    
    y_df = pd.DataFrame(y)
    y_df.columns = ["amplitude"]
    
    y_df["sampling"]=range(0,len(y))
    y_df["time"]=y_df["sampling"] / sr
    
    y_df.plot(x='time', y='amplitude')