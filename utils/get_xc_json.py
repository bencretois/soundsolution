import argparse
import os
import urllib.request
import json


def get_json(query, output_folder):

    url = "https://www.xeno-canto.org/api/2/recordings?query={}".format(query)
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())

    with open(os.path.join(output_folder, '{}.json'.format(query)), 'w+') as f:
        json.dump(data, f)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("--query",
                    help='query',
                    required=True,
                    type=str,
                    )

    parser.add_argument("--output_folder",
                    help='Path to the output folder',
                    required=True,
                    type=str,
                    )

    cli_args = parser.parse_args()

    list_species = open(cli_args.query)
    list_species = json.load(list_species)

    for species in list_species['query']:
        get_json(species, cli_args.output_folder)
        
# python python_scripts/get_xc_json.py --query python_scripts/list_species.json --output_folder api_output

