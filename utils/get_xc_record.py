import argparse
import requests
import json
import os
from pydub import AudioSegment

def get_record(json_file, output_folder):

    f = open(json_file)
    data = json.load(f)

    for item in range(len(data["recordings"])):

        file_name = data["recordings"][item]["file-name"]
        file_to_download = data["recordings"][item]["file"]

        r = requests.get(file_to_download, allow_redirects=True)
        with open(os.path.join(output_folder, file_name), 'wb') as f:
            f.write(r.content)
			
if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--json_file",
                    help='File to the json file containing the queries',
                    required=True,
                    type=str,
                    )

    parser.add_argument("--output_folder",
                    help='Path to the output folder',
                    required=True,
                    type=str,
                    )

    cli_args = parser.parse_args()

    get_record(cli_args.json_file, cli_args.output_folder)


