import numpy as np
import librosa
import librosa.display
import random

from pydub import AudioSegment
from pydub.utils import mediainfo
from audiomentations import Compose, AddGaussianNoise, TimeStretch, PitchShift, Shift
from audiomentations import SpecCompose, SpecFrequencyMask

class AugmentData():

    def __init__(self, p_augmentation=0.8, l_segments = 3000, normalization_value_dBFS = -20, n_fft=2048, hop_length=1024, n_mels=128, sample_rate=16000):

        # Path to the data
        #self.data_path = data_path

        # Dataset info
        #self.info = mediainfo(data_path)
        self.sr = sample_rate
        #self.sr = int(self.info["sample_rate"])
        #self.n_channels = int(self.info["channels"])

        # Audio processing
        self.l_segments = l_segments
        self.normalization_value_dBFS = normalization_value_dBFS

        # Data augmentation
        self.p_augmentation = p_augmentation

        # Related to the spectrogram
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.n_mels = n_mels
        self.len_spectrogram = round(((self.hop_length / self.sr) * self.l_segments) / (self.n_fft / self.hop_length))

    def soundscape2segments_iter(self, record):
        """ Divide the audio track into segments of length l_segment"""
        for index in range(0, len(record), self.l_segments):
            yield record[index:index + self.l_segments]

    def pad_segment_iter(self, iter_segments):
        """Add padding to have a normalized segment length"""
        for segment in iter_segments:
            d = self.l_segments - len(segment)
            silence = AudioSegment.silent(duration = d)
            padded = segment + silence
            yield padded

    def match_target_amplitude(self, iter_segments):
        """ mean RMS normalization"""
        for segment in iter_segments:
            change_in_dBFS = self.normalization_value_dBFS - segment.dBFS
            yield segment.apply_gain(change_in_dBFS)

    def get_array(self, iter_augm):
        """Get the array of the segment"""
        for segment in iter_augm:
            yield np.array(segment.get_array_of_samples(), dtype=np.float32)

    def augment_audio(self, iter_norm):
        """
        Audio augmentation 
        https://github.com/iver56/audiomentations
        """

        augment = Compose([
                AddGaussianNoise(min_amplitude=0.001, max_amplitude=0.015, p=self.p_augmentation),
                TimeStretch(min_rate=0.8, max_rate=1.25, p=self.p_augmentation),
                PitchShift(min_semitones=-4, max_semitones=4, p=self.p_augmentation),
                Shift(min_fraction=-0.5, max_fraction=0.5, p=self.p_augmentation),
        ])

        for segment in iter_norm:
            yield augment(segment, sample_rate=self.sr)

    def to_mel_spectrogram(self, iter_arr):
        """Get the mel spectrogram"""
        for arr in iter_arr:
            sgram = librosa.stft(arr, n_fft=self.n_fft, hop_length=self.hop_length)
            sgram_mag, _ = librosa.magphase(sgram)
            mel_scale_sgram = librosa.feature.melspectrogram(S=sgram_mag, sr=self.sr, n_mels=self.n_mels)
            mel_sgram = librosa.amplitude_to_db(mel_scale_sgram)
            yield mel_sgram

    def augment_spectrogram(self, iter_mel, label):
        """Spectrogram augmentation"""

        augment = SpecCompose(
                [SpecFrequencyMask(p=0.5),])

        for segment in iter_mel:
            yield (augment(segment), label)

    def main(self, data_path):
        # Open the audio file and resample to have stereo and a standard sample rate
        audio = AudioSegment.from_file(data_path)
        audio = audio.set_frame_rate(self.sr)

        # Split the channels into a list & select a single channel
        splitted_audio = audio.split_to_mono()
        segment = random.choice(splitted_audio)

        # List all the folders which are the labels
        label = data_path.split("/")[-2]

        # Apply the pipeline
        iter_segment = self.soundscape2segments_iter(segment)
        iter_segment = self.pad_segment_iter(iter_segment)
        iter_norm = self.match_target_amplitude(iter_segment)
        iter_arr = self.get_array(iter_norm)
        iter_augm = self.augment_audio(iter_arr)
        iter_mel = self.to_mel_spectrogram(iter_augm)
        iter_spec = self.augment_spectrogram(iter_mel, label)

        # Return the iterator
        return iter_spec