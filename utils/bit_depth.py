def get_bit_depth(file):

    bit_depth = file.sample_width  # 1-4. 1=8 2=16 3=24 4=32

    if bit_depth is 1:
        bit_depth = 8

    elif bit_depth is 2:
        bit_depth = 16

    elif bit_depth is 3:
        bit_depth = 24

    elif bit_depth is 4:
        bit_depth = 32

    return bit_depth