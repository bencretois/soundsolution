import glob
import torch
import pytorch_lightning as pl

from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader, random_split
from torch.utils.data import IterableDataset
from itertools import cycle

class EncodeLabels():
    """
    Function that encodes names of folders as numerical labels
    Wrapper around sklearn's LabelEncoder
    """
    def __init__(self, path_to_folders):
        self.path_to_folders = path_to_folders
        self.class_encode = LabelEncoder()
        self._labels_name()

    def _labels_name(self):
        labels = glob.glob(self.path_to_folders)
        labels = [l.split("/")[-1] for l in labels]
        self.class_encode.fit(labels)
        
    def __getLabels__(self):
        return self.class_encode.classes_

    def to_one_hot(self, codec, values):
        value_idxs = codec.transform(values)
        return torch.eye(len(codec.classes_))[value_idxs]

    def one_hot_sample(self, label):
        t_label = self.to_one_hot(self.class_encode, [label])
        return t_label

class AudioLoader(IterableDataset):
    def __init__(self, iterator, label_encoder):
        self.data = iterator
        self.label_encoder = label_encoder

    def process_data(self, data):
        for array, label in data:
            tensor = torch.tensor(array)
            label_encoded = self.label_encoder.one_hot_sample(label)
            label_class = torch.argmax(label_encoded)
            yield (tensor, label_class)

    def get_stream(self, data):
        return iter(self.process_data(data))

    def __iter__(self):
        return self.get_stream(self.data)

class AudioDataModule(pl.LightningDataModule):

    def __init__(self, train_iterator, val_iterator, label_encoder, batch_size, num_workers):
        self.batch_size = batch_size
        self.train_iterator = train_iterator
        self.val_iterator = val_iterator
        self.label_encoder = label_encoder
        self.num_workers = num_workers

    def train_val_loader(self):
        trainLoader = AudioLoader(self.train_iterator, self.label_encoder)
        trainLoader =  DataLoader(trainLoader, batch_size=self.batch_size, num_workers=self.num_workers)

        valLoader = AudioLoader(self.val_iterator, self.label_encoder)
        valLoader = DataLoader(valLoader, batch_size=self.batch_size, num_workers=self.num_workers)

        return (trainLoader, valLoader)

