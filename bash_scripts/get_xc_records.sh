#!/bin/bash

for json_file in api_output/*.json
do
    
    mkdir -p $HOME/Data/soundsolution/$(basename $json_file)

    docker run --rm -it \
            -v $PWD:/app \
            -v $HOME/Data:/Data \
            soundsolution:latest poetry run python python_scripts/get_xc_record.py \
                --json_file $json_file \
                --output_folder /Data/soundsolution/$(basename $json_file)/
done