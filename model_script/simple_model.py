#!/bin/usr/env python

import torch
from torch import nn
from torch.nn import functional as F

class SimpleModel(nn.Module):
    def __init__(self):
        super().__init__()

        self.layer_1 = torch.nn.Linear(128*94, 128)
        self.layer_2 = torch.nn.Linear(128, 256)
        self.layer_3 = torch.nn.Linear(256, 2)

    def forward(self, x):
        batch_size, channels, width, height = x.size()

        # (b, 1, 28, 28) -> (b, 1*28*28)
        x = x.view(batch_size, -1)

        # layer 1
        x = self.layer_1(x)
        x = torch.relu(x)

        # layer 2
        x = self.layer_2(x)
        x = torch.relu(x)

        # layer 3
        x = self.layer_3(x)
        
        # Proba over labels
        x = torch.log_softmax(x, dim=1)

        return x