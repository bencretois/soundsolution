#!/bin/bash

#SBATCH --account=nn5019k --job-name=load_xc_records
#SBATCH --time=24:00:00
#SBATCH --mem-per-cpu=2000M

cd ~/soundsolution

for json_file in api_output/*.json
do

    mkdir -p /cluster/projects/nn5019k/soundsolution/$(basename $json_file)
    
	singularity exec --bind /cluster/projects/nn5019k:/Data \
        soundsolution.sif \
        python python_scripts/get_xc_record.py \
                    --json_file $json_file \
                    --output_folder /Data/soundsolution/$(basename $json_file)/
done





